#!/usr/bin/env python3

'''
	************************************************************
	Michal Charvat - 2018
	Coding challenge - Simple Producer/Consumer Web Link Extractor
	Simple unit tests with mocking requests.get
	************************************************************
'''

import unittest
from unittest import mock
from link_extractor import *

TEST_HTML = '<!DOCTYPE html><html><body><a href="test1"></a><a href="test2"></a></body></html>'

def mock_requests_get(*args, **kwargs):
	class RequestsResponse:
		def __init__(self, content, status_code):
			self.content = content
			self.status_code = status_code

	if args[0] == 'test1':
		return RequestsResponse(TEST_HTML, 200)
	# TODO: add more test cases

	return RequestsResponse('', 404)


class Tests(unittest.TestCase):
	@mock.patch('requests.get', side_effect=mock_requests_get)
	def test_parse_html(self, mock_get):
		queue = Queue(1)
		producer = Producer(None, queue)

		producer.fetch_url('test1')
		fetched_content = queue.get()
		self.assertEqual(str(fetched_content['html']).replace('\n', ''), TEST_HTML)

	@mock.patch('requests.get', side_effect=mock_requests_get)
	def test_get_2_links(self, mock_get):
		queue = Queue(1)
		producer = Producer(None, queue)

		producer.fetch_url('test1')
		fetched_content = queue.get()
		links = extract_links('', fetched_content['html'])
		self.assertEqual(links, ['test1', 'test2'])

	@mock.patch('requests.get', side_effect=mock_requests_get)
	def test_get_invalid_url(self, mock_get):
		queue = Queue(1)
		producer = Producer(None, queue)

		producer.fetch_url('invalid')
		self.assertTrue(queue.empty())

	def test_consumer(self):
		queue = Queue(1)
		message = {'url':'test', 'status_code': 200, 'html': BeautifulSoup(TEST_HTML, 'html.parser')}
		queue.put(message)
		self.assertFalse(queue.empty())

		producer = Producer(None, queue)
		consumer = Consumer(queue, producer)
		consumer.run()

		self.assertTrue(queue.empty())


if __name__ == '__main__':
	unittest.main()