# Producer/Consumer Web Link Extractor
This project is a simple hyper link extractor from given URLs.

## Description
The runs two asynchronous threads:
- `Producer` - Reads input file line by line, for every URL inserts a data fetch task into a ThreadPool.
Every data fetch task run asynchronously for a URL sends a request, parses HTML from a successful HTTP response and adds the parsed HTML into a processing queue.
- `Consumer` - While Producer is producing or the processing queue is not empty, the Consumer pulls the parsed html from the processing queue and
extracts all hyperlinks from the HTML DOM. The hyperlinks are written to the `STDOUT` in a `json` serialized format.


The script can be used with pipes, it does not wait for `EOF` in the input file. It processes the input line by line. And as soon as it finishes processing a URL,
 it writes the result to the `STDOUT`. The execution of the script can be stopped by sending SIGINT signal (2 times).
## Usage
```
\> python3 link_extractor.py [-i <input_file>]
```

- `-i --input <input_file>` optional path to a file containing URLs to be scanned. Every line should contain a URL. If -i argument is not used, the script will read `STDIN`.


## Data example
#### Input file
```
https://edition.cnn.com/
http://fyzika.jreichl.com/
https://www.newyorker.de/cz/fashion/
```

#### Output to STDOUT
```
{"status_code": 200, "url": "https://edition.cnn.com/", "links": ["https://edition.cnn.com/", "https://edition.cnn.com/", "https://edition.cnn.com/world", "https://edition.cnn.com/transcripts", "https://edition.cnn.com/collection", "http://cnnnewsource.com", ...]}
{"status_code": 200, "url": "http://fyzika.jreichl.com/", "links": ["http://fyzika.jreichl.com/", "http://fyzika.jreichl.com/main.search/", "http://fyzika.jreichl.com/main.multimedia/latest/", "http://fyzika.jreichl.com/main.information/about-project/", ...]}
{"status_code": 200, "url": "https://www.newyorker.de/cz/fashion/", "links": ["https://www.newyorker.de/cz/fashion-lifestyle/", "https://www.newyorker.de/cz/fashion/", "https://www.newyorker.de/cz/products/", "https://www.newyorker.de/cz/lifestyle/", "https://jobs.newyorker.de", ...]}

```

## Tests
A few simple unit tests can be found in the `unit_test.py` script.
```
\> python3 unit_test.py
```