#!/usr/bin/env python3

'''
	************************************************************
	Michal Charvat - 2018
	Coding challenge - Simple Producer/Consumer Web Link Extractor
	Usage python3 link_extractor -i <input_file>
	For each input url on a line prints json with url, status_code and found hyperlinks
	************************************************************
'''

from concurrent.futures import ThreadPoolExecutor
from bs4 import BeautifulSoup
from threading import Thread
from queue import Queue, Empty
from urllib.parse import urljoin
import sys
import requests
import json

URL_REQUEST_TIMEOUT = 5
URL_REQUEST_THREADPOOL_SIZE = 5
QUEUE_SIZE = 20
QUEUE_WAIT_TIMEOUT = 1

class Producer(Thread):
	def __init__(self, input_file, output_queue):
		'''
		Producer thread, executed asynchronously. Sends a GET request for every given URL.
		Every successful response is parsed by beautifulsoup and pushed into a queue for processing.
		:param input_file: Path to an input file with URLs stored line by line. When None is passed, STDIN is used.
		:param output_queue: Queue instance. Producer pushes for every successfully fetched URL
				a dict{url:string, status_code:int, html:beautifulsoup instance}
		'''
		self.input_file = input_file
		self.output_queue = output_queue
		Thread.__init__(self)

	def run(self):
		file = sys.stdin

		try:
			if self.input_file is not None:
				file = open(self.input_file, 'r')

			# asynchronous requests
			with ThreadPoolExecutor(max_workers=URL_REQUEST_THREADPOOL_SIZE) as executor:
				for url in file:
					url = url.strip()
					executor.submit(self.fetch_url, (url))

		except IOError:
			sys.stderr.write('Could not open input file.')

		finally:
			file.close()


	def fetch_url(self, url):
		'''
		ThreadPoolWorker, sends a request to an URL, if the response is HTTP_OK,
		it parses html content of the response and pushes it into a queue for processing
		:param url: URL string
		'''
		try:
			fetched_content = {'url': url, 'status_code': None, 'html': None}
			headers = {'User-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36'}

			response = requests.get(url, headers=headers, timeout=URL_REQUEST_TIMEOUT)
			fetched_content['status_code'] = response.status_code
			fetched_content['html'] = BeautifulSoup(response.content, 'html.parser')

			# TODO: improve condition: Content-type:html, other status codes, ...
			if fetched_content['status_code'] != requests.codes.ok:
				return

			if not self.output_queue.full():
				self.output_queue.put(fetched_content)

		# anything goes wrong, we move on
		except Exception as e:
			sys.stderr.write('Exception fetching {}: {}\n'.format(url, str(e)))


class Consumer(Thread):
	def __init__(self, input_queue, producer):
		'''
		Consumer thread, executed asynchronously. Takes one by one parsed html from a queue, extracts all hyperlinks
		and prints them in a json serialized format {url, status_code, links[]} to the stdout.
		:param input_queue: Queue instance, input queue with parsed html objects.
		:param producer: Producer thread reference, used to signalize when Consumer can finish.
		'''
		self.input_queue = input_queue
		self.producer = producer
		Thread.__init__(self)

	def run(self):
		while self.producer.is_alive() or not self.input_queue.empty():
			try:
				fetched_content = self.input_queue.get(block=True, timeout=QUEUE_WAIT_TIMEOUT)

				links = extract_links(fetched_content['url'], fetched_content['html'])
				fetched_content['links'] = links

				fetched_content.pop('html', None) # not needed

				# print output
				print(json.dumps(fetched_content))

			# getting next item timed out or invalid parse
			except Exception:
				pass


def extract_links(url, html_bs):
	links = []
	for link in html_bs.find_all('a', href=True):

		# TODO get rid of javascript links
		links.append(urljoin(url, link['href']))

	return links


def parse_arguments():
	import argparse
	parser = argparse.ArgumentParser(description='Producer/Consumer Link Extractor')
	parser.add_argument('-i', '--input', required=False, help='Input file with URLs.')
	return parser.parse_args()


def main():
	args = parse_arguments()

	markup_queue = Queue(QUEUE_SIZE)

	producer = Producer(args.input, markup_queue)
	consumer = Consumer(markup_queue, producer)

	# kill threads end when main ends (2x SIGINT)
	producer.setDaemon(True)
	consumer.setDaemon(True)

	producer.start()
	consumer.start()

	producer.join()
	consumer.join()

if __name__ == "__main__":
	main()